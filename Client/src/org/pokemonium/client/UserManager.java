package org.pokemonium.client;

import org.pokemonium.client.constants.Language;
import org.pokemonium.client.constants.ServerPacket;
import org.pokemonium.client.network.Whirlpool;
import org.pokemonium.client.protocol.ClientMessage;

/**
 * Generates packets and sends them to the server
 * 
 * @author shadowkanji
 */
public class UserManager
{
	/**
	 * Sends a password change packet
	 * 
	 * @param username
	 * @param newPassword
	 * @param oldPassword
	 */
	public void changePassword(String username, String newPassword, String oldPassword)
	{
		ClientMessage message = new ClientMessage(ServerPacket.CHANGE_PASSWORD);
		message.addString(username + "," + getPasswordHash(username, newPassword) + "," + getPasswordHash(username, oldPassword));
		GameClient.getInstance().getSession().send(message);
	}

	/**
	 * Sends a login packet to server and chat server
	 * 
	 * @param username
	 * @param password
	 */
	public void login(String username, String password)
	{
		byte language = 0;
		switch(GameClient.getInstance().getLanguage()) {
		case Language.ENGLISH:
			language = 0;
			break;
		case Language.PORTUGESE:
			language = 1;
			break;
		case Language.ITALIAN:
			language = 2;
			break;
		case Language.FRENCH:
			language = 3;
			break;
		case Language.FINNISH:
			language = 4;
			break;
		case Language.SPANISH:
			language = 5;
			break;
		case Language.DUTCH:
			language = 6;
			break;
		case Language.GERMAN:
			language = 7;
			break;
		default:
			language = 0;
			break;
		}

		ClientMessage message = new ClientMessage(ServerPacket.LOGIN);
		message.addString(language + username + "," + getPasswordHash(username, password));
		GameClient.getInstance().getSession().send(message);
	}

	/**
	 * Sends a registration packet
	 * 
	 * @param username
	 * @param password
	 * @param email
	 * @param dob
	 * @param starter
	 */
	public void register(String username, String password, String email, String dob, int starter, int sprite, int region)
	{
		ClientMessage message = new ClientMessage(ServerPacket.REGISTRATION);
		message.addInt(region);
		message.addString(username + "," + getPasswordHash(username, password) + "," + email + "," + dob + "," + starter + "," + sprite);
		GameClient.getInstance().getSession().send(message);
	}

	/**
	 * Returns the hashed password
	 * 
	 * @param password
	 * @return
	 */
	private String getPasswordHash(String user, String password)
	{
		String salt = "M0z3ah4SKieNwBboZ94URhIdDbgTNT";
		String user_lowercase = user.toLowerCase();

		// mix the user with the salt to create a unique salt
		String uniqueSalt = "";
		for(int i = 0; i < user_lowercase.length(); i++)
		{
			uniqueSalt += user_lowercase.substring(i, i + 1) + salt.substring(i, i + 1);
			// last iteration, add remaining salt to the end
			if(i == user_lowercase.length() - 1)
				uniqueSalt += salt.substring(i + 1);
		}

		Whirlpool hasher = new Whirlpool();
		hasher.NESSIEinit();

		// add plaintext password with salt to hasher
		hasher.NESSIEadd(password + uniqueSalt);

		// create array to hold the hashed bytes
		byte[] hashed = new byte[64];

		// run the hash
		hasher.NESSIEfinalize(hashed);

		// turn the byte array into a hexstring
		char[] val = new char[2 * hashed.length];
		String hex = "0123456789ABCDEF";
		for(int i = 0; i < hashed.length; i++)
		{
			int b = hashed[i] & 0xff;
			val[2 * i] = hex.charAt(b >>> 4);
			val[2 * i + 1] = hex.charAt(b & 15);
		}
		return String.valueOf(val);
	}
}
